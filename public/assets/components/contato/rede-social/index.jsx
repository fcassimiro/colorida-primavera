import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container, Row, Col, Image } from "react-bootstrap";
import styles from "../../../../../styles/Dev.module.css";
import { SocialIcon } from "react-social-icons";
import { ImWhatsapp  } from 'react-icons/im';

function RedeSocial() {
  return (
    <Container fluid>
      <h4 className={styles.phone}><ImWhatsapp /> 21 96431-5083 </h4>
      <Container className={styles.redeSocial}>
        <Row>
          <Col>
            <SocialIcon
              url="https://www.facebook.com/colorida.primavera1"
              className={styles.iconRede}
              label="Facebook"
              style={{ height: 40, width: 40 }}
              bgColor="#ffffff"
              fgColor="#3B5998"
              target="_blanck"
            />
          </Col>
          <Col>
            <SocialIcon
              url="https://www.instagram.com/coloridaprimavera"
              className={styles.iconRede}
              label="Instagram"
              style={{ height: 40, width: 40 }}
              bgColor="#ffffff"
              fgColor="#E1306C"
              target="_blanck"
            />
          </Col>
        </Row>
      </Container>
    </Container>
  );
}

export default RedeSocial;
