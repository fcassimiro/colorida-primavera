import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container, Row, Col } from "react-bootstrap";
import styles from "../../../../styles/Dev.module.css";
import RedeSocial from "./rede-social";

function Contato() {
  return (
    <Container id="contato" className={styles.contato} fluid>
      <Container className={styles.contatoContent}>
        <Row>
          <Col xs={12} md={6} sm={6} lg={6}>
            <h4 className={styles.titleContato}>Formas de Pagamento</h4>
            <Container className={styles.formaPagamento}>
              <li>Pix</li>
              <li>Transferência</li>
              <li>Boleto bancário</li>
            </Container>
          </Col>
          <Col xs={12} md={6} sm={6} lg={6}>
            <h4 className={styles.titleContato}>Contato</h4>
            <RedeSocial />
          </Col>
        </Row>
      </Container>
    </Container>
  );
}

export default Contato;
