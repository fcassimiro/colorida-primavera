import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Card, Col } from "react-bootstrap";
import styles from "../../../../styles/Dev.module.css";
import Slide from "./slide";

function ItemCard({ card }) {
  const currentMoney = (valor) => {
    return `R$ ${Math.round(valor, 2)}`;
  };
  return (
    <Col xs={12} md={4} sm={6} lg={4}>
      <Card className={styles.card}>
        <Slide card={card} />
        <Card.Body>
          <Card.Title>{card.title}</Card.Title>
          <Card.Text className={styles.descricao}>{card.descricao}</Card.Text>
          <Card.Text className={styles.valor}>
            {currentMoney(card.valor)}
          </Card.Text>
        </Card.Body>
      </Card>
    </Col>
  );
}

export default ItemCard;
