import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Pagination } from "react-bootstrap";
import styles from "../../../../styles/Dev.module.css";

const Paginacao = ({ listProdutos, setPageLimite, active, setActive }) => {
  const [paginas, setPaginas] = useState(1);

  let items = [];
  
  const changeActive = (event) => {
    setActive(parseInt(event.target.textContent));
  };

  for (let number = 1; number <= paginas; number++) {
    items.push(
      <Pagination.Item key={number} active={number === active}>
        {number}
      </Pagination.Item>
    );
  }
  useEffect(() => {
    const inicio = 6 * active - 6;
    const termino = 6 * active - 1;
    setPageLimite([inicio, termino]);
  }, [active, setPageLimite]);

  useEffect(() => {
    const countPage = Math.ceil(listProdutos.length / 6);
    setPaginas(countPage);
  }, [listProdutos, setPaginas]);

  return (
    <div className={styles.pagination}>
      {paginas !== 1 && <Pagination onClick={changeActive}>{items}</Pagination>}
    </div>
  );
};

export default Paginacao;
