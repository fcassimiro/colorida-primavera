import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Carousel, Card } from "react-bootstrap";
import styles from "../../../../styles/Dev.module.css";

const Slide = ({ card }) => {
  const images = (card.images.length > 0) ? card.images : card.img_default;
  return (
    <Carousel fade>
      {images.map((image, key) => (
        <Carousel.Item interval={1500}>
          <Card.Img className={styles.img} variant="top" src={image} />
        </Carousel.Item>
      ))}
    </Carousel>
  );
};

export default Slide;
