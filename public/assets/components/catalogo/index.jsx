import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container, Row } from "react-bootstrap";
import styles from "../../../../styles/Dev.module.css";
import ItemCatalogo from "./item";
import Filter from "./filter";
import Pagination from "./pagination";

function Catalogo() {
  const [produtos, setProdutos] = useState([]);
  const [listProdutos, setListProdutos] = useState([]);
  const [orderType, setOrderType] = useState(0);
  const [filter, setFilter] = useState(0);
  const [pageLimite, setPageLimite] = useState([0, 5]);
  const [active, setActive] = useState(1);

  useEffect(() => {
    async function fetchData() {
      await fetch("/api/produtos", {
        method: "GET",
      })
        .then((res) => res.json())
        .then((data) => {
          setProdutos(data);
          setListProdutos(data);
        })
        .catch((error) => {
          console.log(error);
        });
    }
    fetchData();
  }, []);

  useEffect(() => {
   listProdutos.sort(function (a, b) {
    if (a.title > b.title)
    {
      return 1;
    }
    if (b.title > a.title)
    {
      return -1;
    }
    return 0;
  });
  }, [pageLimite, listProdutos]);

  useEffect(() => {
    const itens = [];
    if (filter != 0) {
      produtos.forEach((produto) => {
        if (produto.categoria === filter) {
          itens.push(produto);
        }
      });
      setListProdutos(itens);
    } else {
      setListProdutos(produtos);
    }
  }, [filter, listProdutos, produtos]);

  useEffect(() => {
    if (orderType == 0) {
      listProdutos.sort((a, b) => {
        return a.valor - b.valor;
      });
    } else {
      listProdutos.sort((a, b) => {
        return b.valor - a.valor;
      });
    }
    setListProdutos(listProdutos);
  }, [orderType, listProdutos]);

  return (
    <>
      <Container id="catalogo" className={styles.catalogo}>
        <h4 className={styles.titleCatalogo}>Catálogo</h4>
        <Filter setOrderType={setOrderType} setFilter={setFilter} setPageLimite={setPageLimite} setActive={setActive}/>
        <Row className={styles.cards}>
          {listProdutos.map((card, key) => (
            (key >= pageLimite[0] && key <= pageLimite[1]) && <ItemCatalogo key={key} card={card}/>
          ))}
        </Row>
        <Pagination listProdutos={listProdutos} setPageLimite={setPageLimite} active={active} setActive={setActive} />
      </Container>
    </>
  );
}

export default Catalogo;
