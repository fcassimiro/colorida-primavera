import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container, Form, Row, Col } from "react-bootstrap";
import styles from "../../../../styles/Dev.module.css";

const Filter = ({ setFilter, setOrderType, setPageLimite, setActive }) => {
  const [categorias, setCategorias] = useState([]);

  useEffect(() => {
    async function fetchData() {
      await fetch("/api/categorias", {
        method: "GET",
      })
        .then((res) => res.json())
        .then((data) => {
          setCategorias(data);
        })
        .catch((error) => {
          console.log(error);
        });
    }
    fetchData();
  }, []);

  const filterProdutos = (event) => {
    const filter = parseInt(event.target.value);
    setFilter(filter);
    setActive(1);
    setPageLimite([0, 5]);
  };

  const orderByProdutos = (event) => {
    const type = parseInt(event.target.value);
    setOrderType(type);
    setActive(1);
    setPageLimite([0, 5]);
  };

  return (
    <Container className={styles.filter} fluid>
      <Row>
        <Col></Col>
        <Col md={5} sm={5} lg={3} xl={3}>
          <Form.Group>
            <Form.Label>Filtrar Categoria</Form.Label>
            <Form.Control onChange={filterProdutos} size="sm" as="select">
              <option value={0}>Todas</option>
              {categorias.map((categoria, key) => (
                <option key={key} value={categoria._id}>
                  {categoria.title}
                </option>
              ))}
            </Form.Control>
          </Form.Group>
        </Col>
        {/*
           <Col md={5} sm={5} lg={3} xl={3}>
          <Form.Group>
            <Form.Label>Ordenar</Form.Label>
            <Form.Control onChange={orderByProdutos} size="sm" as="select">
              <option value={0}>Maior preço</option>
              <option value={1}>Menor preço</option>
            </Form.Control>
          </Form.Group>
        </Col>*/}
      </Row>
    </Container>
  );
};

export default Filter;
