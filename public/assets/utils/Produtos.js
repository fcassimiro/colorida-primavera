const Produtos = [
    {
        _id: 1,
        categoria: 1,
        title: "Mix Flores do Campo",
        descricao: "Buque mix flores do campo.",
        images: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806131/Mix%20flores%20do%20campo/IMG_3977_mzgnzk.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806123/Mix%20flores%20do%20campo/IMG_1541_kvties.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806122/Mix%20flores%20do%20campo/IMG_1415_yk5ajs.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806120/Mix%20flores%20do%20campo/IMG_1477_iemjrg.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806119/Mix%20flores%20do%20campo/IMG_1350_hpfc5d.jpg",
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 105
    },
    {
        _id: 2,
        categoria: 1,
        title: "Mix Bougainville",
        descricao: "Mix desidratado com Bougainville e dedo de anjo.",
        images: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806123/Mix%20bougainville/IMG_3996_q5jcpf.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806117/Mix%20bougainville/IMG_3995_rsmf7d.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806108/Mix%20bougainville/IMG_1766_xrqhwa.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806106/Mix%20bougainville/IMG_1727_lhnqrq.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806103/Mix%20bougainville/IMG_1589_mapzkr.jpg"
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 96
    },
    {
        _id: 3,
        categoria: 1,
        title: "Mix Koke",
        descricao: "Três em um. Mix de gerbera, mini palmeira e crista de galo em uma kokedama.",
        images: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806154/Mix%20koke/IMG_3961_x8clo6.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806153/Mix%20koke/IMG_3963_xrxrd0.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806146/Mix%20koke/IMG_3951_fr28ua.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806145/Mix%20koke/IMG_3962_fhahge.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806138/Mix%20koke/IMG_3959_d3ocez.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806137/Mix%20koke/IMG_3954_t0vgmi.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806136/Mix%20koke/IMG_3957_mhdgeg.jpg"
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 89
    },
    {
        _id: 4,
        categoria: 1,
        title: "Antúrio",
        descricao: "Antúrio vermelho no vaso de cimento ou kokedama.",
        images: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623805728/Anturio/anturio_ok_1_wmvzqq.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623805727/Anturio/anturio_ok_2_wmf7c6.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623805727/Anturio/C%C3%B3pia_de_anturio_ok_1_ds39nu.jpg"
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 72
    }
    ,
    {
        _id: 5,
        categoria: 1,
        title: "Begônia",
        descricao: "Begônia no vaso de cimento ou kokedama. Cores: rosa e vermelha.",
        images: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806100/Beg%C3%B4nia/begonia_ok_3_fwuq5d.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806091/Beg%C3%B4nia/begonia_ok_2_lcb4v1.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806090/Beg%C3%B4nia/begonia_ok_1_ayfsp8.jpg"
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 78
    }
    ,
    {
        _id: 6,
        categoria: 1,
        title: "Peperômia",
        descricao: "Peperômia melancia no vaso de cimento ou kokedama.",
        images: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806150/Peperomia%20melancia/paperomia_m_ok_2_z5myjt.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806149/Peperomia%20melancia/paperomia_m_ok_1_xtquea.jpg"
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 78
    }
    ,
    {
        _id: 7,
        categoria: 1,
        title: "Lirio Laranja",
        descricao: "Lirio laranja no vaso de cimento ou kokedama.",
        images: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806123/L%C3%ADrio%20Laranja/IMG_2223_1_l70ulj.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806117/L%C3%ADrio%20Laranja/IMG_2224_1_hebuo7.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806113/L%C3%ADrio%20Laranja/IMG_2225_1_nftvyz.jpg"
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 82
    }
    ,
    {
        _id: 8,
        categoria: 1,
        title: "Cacto Mandacaru",
        descricao: "Cacto Mandacaru no vaso de cimento ou kokedama.",
        images: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806118/Cacto%20mandacaru/ok_x88b8n.jpg"
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 95
    }
    ,
    {
        _id: 9,
        categoria: 1,
        title: "Azaleia Mini Árvore Grande",
        descricao: "Azaleia mini árvore grande em kokedama.",
        images: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806096/Azaleia%20grande/IMG_3942_pdhxr6.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806092/Azaleia%20grande/IMG_3940_bexvlb.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806084/Azaleia%20grande/IMG_3936_bx2vyf.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806079/Azaleia%20grande/azaleia_1_mini_arvore_wscpae.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806078/Azaleia%20grande/IMG_3940_bhikxh.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806076/Azaleia%20grande/azaleia_detalhe_i9yxtr.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806075/Azaleia%20grande/azaleia_2_rx2xej.jpg"
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 92
    }
    ,
    {
        _id: 10,
        categoria: 1,
        title: "Azaleia Mini Árvore Pequena",
        descricao: "Azaleia mini árvore pequena em kokedama.",
        images: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806091/Azaleia%20pequena/IMG_3937_zbxwwu.jpg"
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 68
    }
    ,
    {
        _id: 11,
        categoria: 1,
        title: "Singônio",
        descricao: "Singônio no vaso de cimento ou kokedama.",
        images: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806157/Sing%C3%B4nio/singonio_editado_2_oitloz.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806153/Sing%C3%B4nio/singonio_editado_1_rkzg9a.jpg"
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 68
    }
    ,
    {
        _id: 12,
        categoria: 1,
        title: "Bougainville",
        descricao: "Bougainville desidratado cores laranja ou rosa chiclete.",
        images: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806106/Bougainville%20desidratado/IMG_3741_qd8bpm.jpg",
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623806099/Bougainville%20desidratado/IMG_3740_uqpoid.jpg"
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 92
    }
    ,
    {
        _id: 13,
        categoria: 1,
        title: "Begônia Rex Frozen",
        descricao: "Preencher este campo.",
        images: [
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 78
    }
    ,
    {
        _id: 14,
        categoria: 1,
        title: "Begônia Rex Vermelha",
        descricao: "Preencher este campo.",
        images: [
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 78
    }
    ,
    {
        _id: 15,
        categoria: 1,
        title: "Begônia Rex Verde Mesclado",
        descricao: "Preencher este campo.",
        images: [
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 78
    }
    ,
    {
        _id: 16,
        categoria: 1,
        title: "Begônia Rex Verde",
        descricao: "Preencher este campo.",
        images: [
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 78
    }
    ,
    {
        _id: 17,
        categoria: 1,
        title: "Dracena",
        descricao: "Preencher este campo.",
        images: [
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 92
    }
    ,
    {
        _id: 18,
        categoria: 1,
        title: "Crotón",
        descricao: "Preencher este campo.",
        images: [
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 95
    }
    ,
    {
        _id: 19,
        categoria: 1,
        title: "Jibóia",
        descricao: "Preencher este campo.",
        images: [
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 98
    }
    ,
    {
        _id: 20,
        categoria: 1,
        title: "Buogainville Duo",
        descricao: "Bougainville rosa chiclete com Bougainville champagne.",
        images: [
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 98
    }
    ,
    {
        _id: 21,
        categoria: 2,
        title: "Suporte Madeira",
        descricao: "Suporte com base em madeira com arame de ferro.",
        images: [
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 52
    }
    ,
    {
        _id: 22,
        categoria: 2,
        title: "Suporte Betânia",
        descricao: "Cachepó de mdf com pote interno.",
        images: [
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 58
    }
    ,
    {
        _id: 23,
        categoria: 2,
        title: "Vaso Ana Granilite",
        descricao: "Vaso artesanal granilite.",
        images: [
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 68
    }
    ,
    {
        _id: 24,
        categoria: 2,
        title: "Suporte Ovo Granilite",
        descricao: "Suporte de cimento artesanal granilite.",
        images: [
        ],
        img_default: [
            "https://res.cloudinary.com/dktrnfmwu/image/upload/v1623807605/logo_o0ouk2.jpg"
        ],
        valor: 32
    }

];

export default Produtos