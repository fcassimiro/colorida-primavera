import React, { Component, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Head from "next/head";
import { Image, Container, Row, Col, Navbar, Nav } from "react-bootstrap";
import styles from "../styles/Home.module.css";

export default function Home() {
  const [show, setShow] = useState(true);
  return (
    <>
      <Head>
        <title>Colorida Primavera</title>
        <link rel="icon" href="assets/favicon.ico" />
      </Head>
      <Container className={styles.main} fluid>
        <Image
          className={styles.logo}
          src="assets/logo.jpeg"
          roundedCircle
          thumbnail
        />
      </Container>
      <footer className={styles.footer}>
        <b> COLORIDA PRIMAVERA 2020 - 2021 </b>
      </footer>
    </>
  );
}
