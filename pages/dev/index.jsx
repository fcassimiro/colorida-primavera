import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Head from "next/head";
import { Image, Container, Row, Col } from "react-bootstrap";
import styles from "../../styles/Dev.module.css";
import Catalogo from "../../public/assets/components/catalogo";
import Contato from "../../public/assets/components/contato";
import { scroller } from "react-scroll";

export default function Home() {
  const [show, setShow] = useState(true);

  const positionScroll = (id) => (event) => {
    scroller.scrollTo(id, {
      duration: 1000,
      delay: 0,
      smooth: "easeInOutQuart",
    });
  };

  return (
    <body className={styles.body}>
      <Head>
        <title>Colorida Primavera</title>
        <link rel="icon" href="assets/favicon.ico" />
      </Head>
      <header className={styles.header}>
        <Image
          className={styles.logo}
          src="assets/logo.jpeg"
          roundedCircle
          thumbnail
        />
      </header>
      <Container className={styles.nav} fluid>
        <li style={{cursor: 'pointer'}} className={styles.li} onClick={positionScroll("home")}>
          Home
        </li>
        <li style={{cursor: 'pointer'}}className={styles.li} onClick={positionScroll("about")}>
          Sobre
        </li>
        <li style={{cursor: 'pointer'}} className={styles.li} onClick={positionScroll("catalogo")}>
          Catálogo
        </li>
        <li style={{cursor: 'pointer'}} className={styles.li} onClick={positionScroll("contato")}>
          Contato
        </li>
      </Container>

      <Container className={styles.home} fluid></Container>

      <Container id="about" className={styles.sobre} fluid>
        <Container className={styles.descricaoSobre}>
          <h4 className={styles.titleSobre}>Sobre</h4>
          Nossa empresa tem como ideia a produção artesanal e familiar. Colorida
          Primavera tem o objetivo de trazer mais beleza para a vida das pessoas
          através da natureza, deixando seu dia a dia mais verde. Nós oferecemos
          serviços de paisagismo, jardinagem e hortas além da produção e
          comercialização de flores e plantas em Kokedamas. Também fornecemos
          vasos e suportes artesanais.
        </Container>
      </Container>

      <Catalogo />

      <Contato />

      <footer className={styles.footer}>
        <b> COLORIDA PRIMAVERA 2020 - 2021 </b>
      </footer>
    </body>
  );
}
