// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import produtos from "../../../public/assets/utils/Produtos"

export default (req, res) => {
  res.statusCode = 200
  produtos.sort(function (a, b) {
    if (a.title > b.title)
    {
      return 1;
    }
    if (b.title > a.title)
    {
      return -1;
    }
    return 0;
  });
  res.json(produtos)
}
