// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import categorias from "../../../public/assets/utils/Categorias"

export default (req, res) => {
  res.statusCode = 200
  res.json(categorias)
}
